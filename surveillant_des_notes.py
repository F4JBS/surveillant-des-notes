#!/usr/bin/python3

# IMPORTATION LIBS
import mechanize
import discord
import configparser
from asyncio import sleep
from os import system
from os import path as os_path
from datetime import datetime

# RECUPERATION CONFIGURATION
config = configparser.ConfigParser()
PATH = os_path.abspath(os_path.split(__file__)[0])
config.read(PATH + "/config.ini")
dir = config['FILES']['dir']
if not dir.endswith("/"):
   dir = dir + "/"

# DEFINITION FONCTIONS
#   Affiche un message de log avec horodatage et l'enregistre dans un fichier défini
def log(message):
   global config
   file = open(dir + config['FILES']['log_file'], "a")
   file.write(datetime.now().strftime("%d/%m/%Y %H:%M") + " : " + message + "\n")
   file.close()
   print(datetime.now().strftime("%d/%m/%Y %H:%M") + " : " + message)

#   Se connecte à RVN et récupère la page html contenant le tableau avec les notes
def recup_actual():
   global config
   br = mechanize.Browser()
   br.set_handle_robots(False)
   br.open(config['AGALAN']['rvn_url'])
   br.select_form(id=config['AGALAN']['form_id'])
   br["username"] = config['AGALAN']['username']
   br["password"] = config['AGALAN']['password']
   res = br.submit()
   content = res.read().decode("utf-8")
   file = open(dir + config['FILES']['actual_file'], "w")
   file.write(content)
   file.close()

#   Extrait du fichier html la liste des matières présentes dans le tableau, retourne cette liste
def extract_liste(html):
   liste_mat = []
   liste = html.split("	    ")
   for mat in liste:
      liste_mat.append(mat.split(" ")[0])
   return liste_mat[1:]

#   Extrait du fichier old la liste des matières déjà publiée, retourne cette liste
def extract_old(raw):
   raw = raw.replace("\n","")
   liste = raw.split(",")
   return liste[0:len(liste)-1]

#   Extrait les listes des matières et les compare, retourne une liste contenant les nouvelles matières
def nouvelles_notes():
   global config
   global actual_list
   global old_list
   try:
      file = open(dir + config['FILES']['old_file'], "r")
      old = file.read()
      file.close()
      old_list = extract_old(old)
   except FileNotFoundError:
      old_list = []

   file = open(dir + config['FILES']['actual_file'], "r")
   actual = file.read()
   file.close()
   actual_list = extract_liste(actual)

   new = []
   for mat in actual_list:
      if not mat in old_list:
         new.append(mat)
   return new

#   Retourne la couleur associée à cette matière, à défaut retourne du blanc (erreur)
def couleur(mat):
   global config
   for ue in config['UE']:
      if mat in config['UE'][ue]:
         return int(config['UE'][ue].split(':')[0])
   return 16777215

#   Retourne un Embed Discord avec le message annonçant une nouvelle note
def msg(mat):
   embed = discord.Embed(title="La moyenne de " + mat + " vient d'être publiée", description="Tu peux la retrouver en suivant le lien suivant : https://rvn.grenet.fr/inp/", color=couleur(mat))
   return embed

#   Copie le contenu du fichier actual et l'écrit dans le fichier old (sale)
def write_old():
   global config
   global actual_list
   global old_list
   actual = ""
   for mat in actual_list:
      if not mat in old_list:
         actual += mat + ","
   file = open(dir + config['FILES']['old_file'], "a")
   file.write(actual)
   file.close()

#   Supprime les fichiers temporaires
def purge():
   global config
   system("rm " + dir + config['FILES']['actual_file'])

# PROGRAMME
client = discord.Client()

async def recurring_task():
   await client.wait_until_ready()
   await sleep(5)
   while not client.is_closed():
      global config
      log("Ready")
      recup_actual()
      log("Bonne récupération des notes actuelles")
      channel = client.get_channel(int(config['DISCORD']['channel_id']))
      nouvelles = nouvelles_notes()
      if len(nouvelles) > 0:
         log("Nouvelles notes détectées, @here")
         await channel.send("<@&" + config['DISCORD']['role_id'] + ">")
      for mat in nouvelles:
         log("Nouvelle note : " + mat)
         await channel.send(embed=msg(mat))
         log("Alerte envoyée pour : " + mat)
      write_old()
      log("Mise à jour old.csv")
      purge()
      log("Nettoyage effectué")
      log("Sleep...")
      await client.change_presence(activity=discord.Game(name=config['DISCORD']['STATUS']))
      await sleep(int(config['AGALAN']['DELAI']))
      log("End")

@client.event
async def on_ready():
   log("Démarrage...")
   await client.change_presence(activity=discord.Game(name=config['DISCORD']['STATUS']))

client.loop.create_task(recurring_task())
client.run(config['DISCORD']['token'])
